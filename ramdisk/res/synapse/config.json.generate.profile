#!/system/bin/sh

cat << CTAG
{
    name:PROFILE,
    elements:[

	{ STitleBar:{
		title:"Kernel Image"
	}},
	{ SOptionList:{
		title:"Selected Image",
        description:" Choose the image you want. APPLY & REBOOT.",
		action:"restorebackup pickboot",
        default:"None",
		values:[ "None",
`
			for IMG in \`/res/synapse/actions/restorebackup listboot\`; do
			  echo "\"$IMG\","
			done
`
		]
	}},
    { STitleBar:{
		title:"Kernel Actions",
    }},
    { SButton:{
		label:"Restore Selected Kernel",
		action:"restorebackup flashboot /dev/block/platform/msm_sdcc.1/by-name/boot"
	}},
    { SButton:{
		label:"Delete Selected Kernel",
		action:"restorebackup delboot"
	}},
	{ SButton:{
		label:"Backup Current Kernel",
		action:"restorebackup keepboot /dev/block/platform/msm_sdcc.1/by-name/boot"
	}},
	{ STitleBar:{
		title:"Configuration Profiles"
	}},
	{ SOptionList:{
		title:"Selected Profile",
        description:" Choose the settings backup you want. APPLY & REBOOT.",
		action:"restorebackup pickconfig",
        default:"None",
		values:[ "None",
`
			for BAK in \`/res/synapse/actions/restorebackup listconfig\`; do
			  echo "\"$BAK\","
			done
`
		]
	}},
    { STitleBar:{
		title:"Settings Actions",
    }},
    { SButton:{
		label:"Restore Selected Profile",
		action:"restorebackup applyconfig"
	}},
    { SButton:{
		label:"Delete Selected Profile",
		action:"restorebackup delconfig"
	}},
	{ SButton:{
		label:"Backup Current Profile",
		action:"restorebackup keepconfig"
	}},
	{ SPane:{
		title:"General Actions",
		description:"To update/refresh lists, select Restart Synapse below."
	}},
	{ SButton:{
		label:"Restart Synapse",
		action:"restorebackup restart"
	}},
    ]
}
CTAG
