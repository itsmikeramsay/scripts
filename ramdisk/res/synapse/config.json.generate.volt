#!/system/bin/sh

cat << CTAG
{
    name:VOLTAGE,
    elements:[
	{ STitleBar:{
		title:"CPU Voltage Control",
	}},
	`if [ -f "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table" ]; then
	
		echo '{ SPane:{
			title:"CPU Global Voltage"
		}},
			{ SSeekBar:{
				default:0,
				action:"voltage cpuvoltglobal global",
				unit:" mV",
				min:-300,
				max:300,
				step:25,
				notify:['
					while read CPUFREQ VOLT UNIT; do
						echo '{
							on:APPLY,
							do:[ REFRESH, APPLY ],
							to:"voltage cpuvolt '${CPUFREQ}'"
						},'
					done < /sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table;
				echo ']
			}},'
	fi`
	`if [ -f "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table" ]; then

		echo '{ SPane:{
			title:"CPU Frequency Voltage",
			description:"Decreasing voltages will lower heat and power consumption of the CPU. Increasing it on overclocked frequencies improves stability. Too low voltages may cause instability and freezes."
		}},'
			while read CPUFREQ VOLT UNIT; do
				echo '{ SSeekBar:{
					title:"'${CPUFREQ//mhz\:/}' MHz",
					unit:"' ${UNIT}'",
					min:600,
					max:1200,
					step:5,
					default:'$VOLT',
					action:"voltage cpuvolt '${CPUFREQ}'"
				}},
				';
					done < /sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table;
	fi`
	{ SButton:{
		label:"Show Voltage Table",
		action:"show cpuvolt"
	}},
    ]
}
CTAG
