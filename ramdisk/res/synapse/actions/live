BB=/res/synapse/busybox;

case $1 in
	battemp)
		BAT_C=`cat /sys/class/power_supply/battery/temp`
		BAT_C=`awk "BEGIN { print ( $BAT_C / 10 ) }"`
		BAT_F=`awk "BEGIN { print ( ($BAT_C * 1.8) + 32 ) }"`
		BAT_H=`cat /sys/class/power_supply/battery/health`;

		echo "$BAT_C°C | $BAT_F°F@nHealth: $BAT_H";
		;;
	cputemp)
		CPU_C=`cat /sys/class/thermal/thermal_zone7/temp`;
		CPU_F=`awk "BEGIN { print ( ($CPU_C * 1.8) + 32 ) }"`

		echo "$CPU_C°C | $CPU_F°F";
		;;
	memory)
		while read TYPE MEM KB; do
			if [ "$TYPE" == "MemTotal:" ]; then
				TOTAL="$((MEM / 1024)) MB"
			elif [ "$TYPE" = "MemFree:" ]; then
				CACHED=$((MEM / 1024))
			elif [ "$TYPE" = "Cached:" ]; then
				FREE=$((MEM / 1024))
			fi
		done < /proc/meminfo;
		
		FREE="$((FREE + CACHED)) MB"
		echo "Total: $TOTAL@nFree: $FREE"
		;;
	uptime)
		TOTAL=`awk '{ print $1 }' /proc/uptime`;
		AWAKE=$((`awk '{s+=$2} END {print s}' /sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state` / 100));
		SLEEP=`awk "BEGIN { print ($TOTAL - $AWAKE) }"`;
		
		PERC_A=`awk "BEGIN { print ( ($AWAKE / $TOTAL) * 100) }"`
		PERC_A="`printf "%0.1f\n" $PERC_A`%"
		PERC_S=`awk "BEGIN { print ( ($SLEEP / $TOTAL) * 100) }"`
		PERC_S="`printf "%0.1f\n" $PERC_S`%"
		
		TOTAL=`echo - | awk -v "S=$TOTAL" '{printf "%dh:%dm:%ds",S/(60*60),S%(60*60)/60,S%60}'`
		AWAKE=`echo - | awk -v "S=$AWAKE" '{printf "%dh:%dm:%ds",S/(60*60),S%(60*60)/60,S%60}'`
		SLEEP=`echo - | awk -v "S=$SLEEP" '{printf "%dh:%dm:%ds",S/(60*60),S%(60*60)/60,S%60}'`
		echo "Total: $TOTAL (100.0%)@nSleep: $SLEEP ($PERC_S)@nAwake: $AWAKE ($PERC_A)"
		;;
	unused)
		UNUSED="";
		while read FREQ TIME; do
			FREQ="$((FREQ / 1000)) MHz"
			if [ $TIME -lt "100" ]; then
				UNUSED="$UNUSED$FREQ, "
			fi
		done < /sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state;
		
		UNUSED=${UNUSED%??}
		echo "$UNUSED"
		;;
	time)
		STATE="";
		CNT=0;
		SUM=`awk '{s+=$2} END {print s}' /sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state`;
		
		while read FREQ TIME; do
			if [ "$CNT" -ge $2 ] && [ "$CNT" -le $3 ]; then
				FREQ="$((FREQ / 1000)) MHz"
				if [ $TIME -ge "100" ]; then
					PERC=`awk "BEGIN { print ( ($TIME / $SUM) * 100) }"`
					PERC="`printf "%0.1f\n" $PERC`%"
					TIME=$((TIME / 100))
					STATE="$STATE $FREQ - `echo - | awk -v "S=$TIME" '{printf "%dh:%dm:%ds",S/(60*60),S%(60*60)/60,S%60}'` - ($PERC)@n"
				fi
			fi
			CNT=$((CNT+1))
		done < /sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state;
		
		STATE=${STATE%??}
		echo "$STATE"
		;;
	cpufreq)
		CPU0=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq 2> /dev/null`
		CPU1=`cat /sys/devices/system/cpu/cpu1/cpufreq/scaling_cur_freq 2> /dev/null`
		CPU2=`cat /sys/devices/system/cpu/cpu2/cpufreq/scaling_cur_freq 2> /dev/null`
		CPU3=`cat /sys/devices/system/cpu/cpu3/cpufreq/scaling_cur_freq 2> /dev/null`
		
		$BB test -z $CPU0 && CPU0="Offline";
		$BB test -z $CPU1 && CPU1="Offline";
		$BB test -z $CPU2 && CPU2="Offline";
		$BB test -z $CPU3 && CPU3="Offline";
		
		$BB test $CPU0 != "Offline" && CPU0="$((CPU0 / 1000)) MHz";
		$BB test $CPU1 != "Offline" && CPU1="$((CPU1 / 1000)) MHz";
		$BB test $CPU2 != "Offline" && CPU2="$((CPU2 / 1000)) MHz";
		$BB test $CPU3 != "Offline" && CPU3="$((CPU3 / 1000)) MHz";
		
		echo "Core 0: $CPU0@nCore 1: $CPU1@nCore 2: $CPU2@nCore 3: $CPU3";
		;;
	gpufreq)
		GPUFREQ="$((`cat /sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0/gpuclk` / 1000000)) MHz";
		echo "$GPUFREQ";
		;;
	bootloader)
		version=`getprop ro.bootloader`;
		block=/dev/block/platform/msm_sdcc.1/by-name/misc;
		lockstate=`$BB dd ibs=1 count=1 skip=16400 if=$block 2> /dev/null | $BB od -h | $BB head -n 1 | $BB cut -c 11-`;
		tamperstate=`$BB dd ibs=1 count=1 skip=16404 if=$block 2> /dev/null | $BB od -h | $BB head -n 1 | $BB cut -c 11-`;
		
		if [ $lockstate == "00" ]; then
			state="Locked";
		elif [ $lockstate == "01" ]; then
			state="Unlocked";
		else
			state="Unknown";
		fi;
		
		if [ $tamperstate == "00" ]; then
			tamper="False";
		elif [ $tamperstate == "01" ]; then
			tamper="True";
		else
			tamper="Unknown";
		fi;
		
		echo "Version: $version@nState: $state@nTamper: $tamper"
		;;
	entropy)
	        echo "`cat /proc/sys/kernel/random/entropy_avail`";
	        ;;
esac;
